package ihpc;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.net.http.HttpResponse;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base32;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.credential.DefaultPasswordService;
import org.json.JSONObject;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;


/**
 * Servlet implementation class WebApiUser
 */
@WebServlet("/webapi/users")
public class WebApiUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WebApiUser() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    private String generateSecretKey() {
	    SecureRandom random = new SecureRandom();
	    byte[] bytes = new byte[20];
	    random.nextBytes(bytes);
	    Base32 base32 = new Base32();
	    return base32.encodeToString(bytes);
	}
    
    private void getQrcode(String key, String username,ServletOutputStream out) {
        
    	String account ="Whatsapp Bot";
		
		try {
	        String barcode= "otpauth://totp/"
	                + URLEncoder.encode(username, "UTF-8")
	                + "?secret=" + URLEncoder.encode(key, "UTF-8")
	                + "&issuer=" + URLEncoder.encode(account, "UTF-8");
	        
	        int height = 177;
		    int width = 177;
		    BitMatrix matrix = new MultiFormatWriter().encode(barcode, BarcodeFormat.QR_CODE,
		            width, height);
		    MatrixToImageWriter.writeToStream(matrix, "png", out);
		    
	    } catch (WriterException | IOException e) {
	        throw new IllegalStateException(e);
	    }
    	return;
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        
        ApiConnection api = new ApiConnection();
        
        
        if (request.getParameter("me") != null) {
        
        	String login_user_id = request.getUserPrincipal().getName();
        	Log.info(login_user_id);
        	String condition = "_cond="+Base64.getEncoder().withoutPadding().encodeToString(("name = '"+login_user_id+"'").getBytes());
        	String fields = "_fields="+Base64.getEncoder().withoutPadding().encodeToString(("id,name,email,roleAdmin").getBytes());
        	
        	String login_user = api.QueryTable("user?"+condition+"&"+fields);
        	Log.info(login_user);
        	
        	response.getWriter().print(login_user);
            response.getWriter().flush();
            
            return;
        	
        }
        
		if (!SecurityUtils.getSubject().hasRole("admin")) {
        	response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        	return;
        }
		
		
		String users = "[]";
		
		String fields = "?_fields="+Base64.getEncoder().withoutPadding().encodeToString(("id,name,email,roleAdmin").getBytes());
		String condition = "_cond="+Base64.getEncoder().withoutPadding().encodeToString(("id = "+request.getParameter("id")).getBytes());
		String orderBy = "_orderBy="+Base64.getEncoder().withoutPadding().encodeToString(("id desc").getBytes());
		
		if (request.getParameter("qrcode") != null) {
			users = api.QueryTable("user/"+request.getParameter("id"));
			JSONObject uu = new JSONObject(users);
			response.setContentType("image/png");
			response.setHeader("Content-disposition", "attachment; filename="+uu.getString("name")+".png");
			getQrcode(uu.getString("secretKey"),uu.getString("name"),response.getOutputStream());
			Log.info(users,users);			
			return;
		}
		if (request.getParameter("id") == null)
		   users = api.QueryTable("user"+fields+"&"+orderBy);
		else		
		   users = api.QueryTable("user"+fields+"&"+condition);	

        response.getWriter().print(users);
        response.getWriter().flush();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
     
        if (!SecurityUtils.getSubject().hasRole("admin")) {
        	response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        	return;
        }
        
        if (request.getParameter("del") != null) {
        	
        	ApiConnection delapi = new ApiConnection();
    		int ret = delapi.DeleteUser(request.getParameter("id"));
    		response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.setStatus(ret);
            return;
        }
        
	 if (request.getParameter("password") != null) {
		String patten = "((?=.*[a-z])(?=.*\\d)(?=.*[A-Z])(?=.*[@#$%!^*&]).{15,})";
		Pattern pattern = Pattern.compile(patten);
		Matcher matcher = pattern.matcher(request.getParameter("password"));
		if (matcher.matches() == false) {
			//response.sendRedirect("passwd.html");
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
  			response.getWriter().write("{\"msg\":\"invalid password\"}");
			return;
		}
		
	  }
	 
	 ApiConnection api = new ApiConnection();
	 int id = Integer.parseInt(request.getParameter("id"));
	 String password = null;
	 if (request.getParameter("password") != null) {
		
		 DefaultPasswordService defaultPassSvc = new DefaultPasswordService();
		 password =  defaultPassSvc.encryptPassword(request.getParameter("password"));
		 
	 }
	 String secretkey = null;
	 if (id == 0)
		 secretkey = generateSecretKey();
	 
	 Log.info(request.getParameter("id"));	 
	 
	 
	try {
		HttpResponse<String> ret = api.UpdateAndUser(id, password, request.getParameter("name"), request.getParameter("email"), request.getParameter("adminflag"), secretkey);
		response.setStatus(ret.statusCode());
		response.getWriter().write("{\"msg\":\""+ret.body()+"\"}");
	} catch (Exception  e) {
		// TODO Auto-generated catch block
		response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        response.getWriter().print(e.getMessage());
        response.getWriter().flush();
	 }	 
		
	}

	
}
